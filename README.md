# AWX Debian install

Ansible role to install AWX on Debian.

**Note : work in progress, doesn't work for now.**

## Requirements

* Ansible >= 4

## OS

* Debian (Bookwom)

## Role Variables

Variable name         | Variable description     | Type    | Default | Required
---                   | ---                      | ---     | ---     | ---
awx_repo_url          | URL of AWX repo          | string  | yes     | yes
awx_repo_pubkey       | Repo public key          | string  | yes     | yes
awx_repo_keyring      | AWX repo keyring         | string  | yes     | yes
awx_required_packages | Base packages to install | string  | yes     | yes
awx_awx_packages      | AWX packages to install  | string  | yes     | yes

## Dependencies

* No

## Playbook Example

```
To become
```

## License

GPL v3

## Links

* [Ansible documentation](https://docs.ansible.com/ansible/latest/)
* [AWX](https://www.ansible.com/products/awx-project)

## Author Information

* [Stephane Paillet](mailto:spaillet@ethicsys.fr)
